--------------------------------------------------------
--  DDL for Table XX_AP_TERMS_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_AP_TERMS_SETUP" 
   (	"TERM_ID" NUMBER, 
	"NAME" VARCHAR2(30), 
	"START_DATE_ACTIVE" DATE, 
	"END_DATE_ACTIVE" DATE, 
	"ENABLED_FLAG" VARCHAR2(1), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
