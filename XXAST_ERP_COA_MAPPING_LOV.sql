--------------------------------------------------------
--  DDL for Table XXAST_ERP_COA_MAPPING_LOV
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XXAST_ERP_COA_MAPPING_LOV" 
   (	"TYPE" VARCHAR2(80), 
	"NAME" VARCHAR2(255), 
	"VALUE" VARCHAR2(255), 
	"CREATION_DATE" DATE, 
	"STATUS" VARCHAR2(255)
   ) ;
