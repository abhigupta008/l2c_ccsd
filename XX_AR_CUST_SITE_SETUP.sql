--------------------------------------------------------
--  DDL for Table XX_AR_CUST_SITE_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_AR_CUST_SITE_SETUP" 
   (	"ACCT_REF" VARCHAR2(150), 
	"SITE_REF" VARCHAR2(150), 
	"ORIG_SYSTEM_BILL_TO_REF" VARCHAR2(150), 
	"ORIG_SYSTEM_BILL_TO_SITE_REF" VARCHAR2(150), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
