--------------------------------------------------------
--  DDL for Table XX_AP_TAX_TYPES_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_AP_TAX_TYPES_SETUP" 
   (	"INCOME_TAX_TYPE" VARCHAR2(30), 
	"DESCRIPTION" VARCHAR2(240), 
	"INACTIVE_DATE" DATE, 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
