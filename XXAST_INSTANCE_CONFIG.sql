--------------------------------------------------------
--  DDL for Table XXAST_INSTANCE_CONFIG
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XXAST_INSTANCE_CONFIG" 
   (	"ID" NUMBER, 
	"PARAM_KEY" VARCHAR2(360), 
	"PARAM_VALUE" VARCHAR2(360), 
	"TYPE" VARCHAR2(50), 
	"DESCRIPTION" VARCHAR2(256), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
