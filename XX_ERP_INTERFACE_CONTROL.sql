--------------------------------------------------------
--  DDL for Table XX_ERP_INTERFACE_CONTROL
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_ERP_INTERFACE_CONTROL" 
   (	"ID" VARCHAR2(80), 
	"INTERFACE_NAME" VARCHAR2(255), 
	"FILE_NAME" VARCHAR2(255), 
	"UNIQUE_ID" VARCHAR2(80), 
	"CREATION_DATE" DATE, 
	"START_TIME" TIMESTAMP (6), 
	"END_TIME" TIMESTAMP (6), 
	"TOTAL_TIME" VARCHAR2(255), 
	"ERROR" VARCHAR2(255), 
	"STATUS" VARCHAR2(255)
   ) ;
