--------------------------------------------------------
--  DDL for Table XX_AR_SITE_USE_LOOKUP_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_AR_SITE_USE_LOOKUP_SETUP" 
   (	"LOOKUP_CODE" VARCHAR2(20), 
	"MEANING" VARCHAR2(100), 
	"ENABLED_FLAG" VARCHAR2(2), 
	"START_DATE_ACTIVE" DATE, 
	"END_DATE_ACTIVE" DATE, 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
