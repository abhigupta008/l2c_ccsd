--------------------------------------------------------
--  DDL for Table XX_GL_JE_SOURCE_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_GL_JE_SOURCE_SETUP" 
   (	"JE_SOURCE_NAME" VARCHAR2(25), 
	"JE_SOURCE_KEY" VARCHAR2(25), 
	"DESCRIPTION" VARCHAR2(240), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
