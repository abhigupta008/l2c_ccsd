--------------------------------------------------------
--  DDL for Table XX_PO_SPO_MASTER
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_PO_SPO_MASTER" 
   (	"PO_NUMBER" VARCHAR2(80), 
	"LINE_NUM" NUMBER, 
	"SHIPMENT_NUM" NUMBER, 
	"DISTRIBUTION_NUM" NUMBER, 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
