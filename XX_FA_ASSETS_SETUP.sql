--------------------------------------------------------
--  DDL for Table XX_FA_ASSETS_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_FA_ASSETS_SETUP" 
   (	"ASSET_NUMBER" VARCHAR2(100), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
