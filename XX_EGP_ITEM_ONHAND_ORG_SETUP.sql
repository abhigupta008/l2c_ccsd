--------------------------------------------------------
--  DDL for Table XX_EGP_ITEM_ONHAND_ORG_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_EGP_ITEM_ONHAND_ORG_SETUP" 
   (	"INV_ORG_ID" NUMBER, 
	"INV_ORG_NAME" VARCHAR2(250), 
	"INV_ORG_CODE" VARCHAR2(10), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
