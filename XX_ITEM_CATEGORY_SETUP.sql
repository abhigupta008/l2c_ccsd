--------------------------------------------------------
--  DDL for Table XX_ITEM_CATEGORY_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_ITEM_CATEGORY_SETUP" 
   (	"CATEGORY_SET_NAME" VARCHAR2(100), 
	"CATEGORY_CODE" VARCHAR2(100), 
	"CATEGORY_NAME" VARCHAR2(1000), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
