--------------------------------------------------------
--  DDL for Table XX_PO_AP_LOOKUP_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_PO_AP_LOOKUP_SETUP" 
   (	"LOOKUP_TYPE" VARCHAR2(50), 
	"LOOKUP_CODE" VARCHAR2(240), 
	"MEANING" VARCHAR2(500), 
	"ENABLED_FLAG" VARCHAR2(1), 
	"START_DATE_ACTIVE" DATE, 
	"END_DATE_ACTIVE" DATE, 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
