--------------------------------------------------------
--  DDL for Table XXAST_INTERFACE_CRON_ENTRIES
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XXAST_INTERFACE_CRON_ENTRIES" 
   (	"ID" NUMBER, 
	"INTERFACE_KEY" VARCHAR2(200), 
	"TYPE" VARCHAR2(10), 
	"CRON_ENTRY" VARCHAR2(200), 
	"STATUS" VARCHAR2(1), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
