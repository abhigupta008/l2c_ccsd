--------------------------------------------------------
--  DDL for Table XX_CUST_TRX_TYPES_MASTER
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_CUST_TRX_TYPES_MASTER" 
   (	"NAME" VARCHAR2(50), 
	"DESCRIPTION" VARCHAR2(150), 
	"TAX_CALCULATION_FLAG" VARCHAR2(5), 
	"START_DATE" DATE, 
	"END_DATE" DATE, 
	"STATUS" VARCHAR2(5), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
