--------------------------------------------------------
--  DDL for Table XX_BU_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_BU_SETUP" 
   (	"BU_NAME" VARCHAR2(60), 
	"SHORT_CODE" VARCHAR2(60), 
	"STATUS" VARCHAR2(1), 
	"MODULE_NAME" VARCHAR2(60), 
	"CONFIGURATION_STATUS" VARCHAR2(1), 
	"FROM_DATE" DATE, 
	"TO_DATE" DATE, 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
