--------------------------------------------------------
--  DDL for Table XX_AP_BANK_LOOKUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_AP_BANK_LOOKUP" 
   (	"LOOKUP_TYPE" VARCHAR2(100), 
	"LOOKUP_CODE" VARCHAR2(100), 
	"MEANING" VARCHAR2(100), 
	"ENABLED_FLAG" VARCHAR2(2), 
	"START_DATE_ACTIVE" DATE, 
	"END_DATE_ACTIVE" DATE, 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
