--------------------------------------------------------
--  DDL for Table XXAST_IFACE_PLSQL_DETAILS_BKUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XXAST_IFACE_PLSQL_DETAILS_BKUP" 
   (	"ID" NUMBER, 
	"INTERFACE_KEY" VARCHAR2(360), 
	"SHEET_NAME" VARCHAR2(360), 
	"SHEET_STG_TABLE_NAME" VARCHAR2(360), 
	"SHEET_VO_TABLE_NAME" VARCHAR2(360), 
	"COLUMN_INDEX_CHANGE_LIST" VARCHAR2(360), 
	"COLUMN_INDEX_CHANGE_VALUE" VARCHAR2(360), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE, 
	"DATA_FILE_NAME" VARCHAR2(360)
   ) ;
