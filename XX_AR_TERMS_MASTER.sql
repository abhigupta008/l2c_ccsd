--------------------------------------------------------
--  DDL for Table XX_AR_TERMS_MASTER
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_AR_TERMS_MASTER" 
   (	"NAME" VARCHAR2(150), 
	"DESCRIPTION" VARCHAR2(2000), 
	"START_DATE_ACTIVE" DATE, 
	"END_DATE_ACTIVE" DATE, 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
