--------------------------------------------------------
--  DDL for Table XXAST_HZ_PARTIES_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XXAST_HZ_PARTIES_SETUP" 
   (	"PARTY_NAME" VARCHAR2(360), 
	"ORIG_SYSTEM_REFERENCE" VARCHAR2(240), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
