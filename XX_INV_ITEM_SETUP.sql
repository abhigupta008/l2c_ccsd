--------------------------------------------------------
--  DDL for Table XX_INV_ITEM_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_INV_ITEM_SETUP" 
   (	"TEMPLATE_COLUMN_NAME" VARCHAR2(400), 
	"LOOKUP_CODE" VARCHAR2(400), 
	"MEANING" VARCHAR2(400), 
	"LOOKUP_TYPE" VARCHAR2(400), 
	"CREATED_BY" VARCHAR2(400), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(400), 
	"LAST_UPDATE_DATE" DATE
   ) ;
