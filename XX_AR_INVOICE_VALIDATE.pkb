create or replace PACKAGE BODY  XX_AR_INVOICE_VALIDATE AS
     
PROCEDURE  invoice_validate (
        source_1           IN      VARCHAR2,
        x_errbuf           OUT     VARCHAR2,
        x_retcode          OUT     NUMBER)  AS 

BEGIN

    DBMS_OUTPUT.PUT_LINE ('Inside invoice_validate');
    invoice_line_validate(source_1);
  --  salescredits_validate(source_1);
  --  invoice_distributions_validate(source_1);
  --  line_contingencies_validate(source_1);

END invoice_validate;

PROCEDURE  invoice_line_validate ( source_1  IN VARCHAR2 )    AS 

    CURSOR line_cur IS
    SELECT ROWID,stg.* 
    FROM XX_RA_INT_LINES_ALL_STG stg
    WHERE source_1 = source_1
    AND nvl(validation_flag, 'N') != 'S'
    order by link_to_line_attribute2 , link_to_line_attribute3; 

    l_error_message VARCHAR2(5000) ;
    lv_org_id  NUMBER ;
    l_count    NUMBER ;
    l_count_init NUMBER := NULL ; 
    lv_trx_type  VARCHAR2(30) ; 
    lv_term_name  VARCHAR2(30) ; 
    lv_orig_system_reference  VARCHAR2(30) ; 
    lv_account_number   VARCHAR2(30) ; 
    lv_party_orig_system_ref   VARCHAR2(30) ; 
    lv_site_orig_system_reference   VARCHAR2(30) ; 
    l_bill_to_cust_num   VARCHAR2(100);
    l_bill_to_cust_site_num  VARCHAR2(100);
    

BEGIN
 --   
   --
    FOR line_rec IN line_cur
    LOOP 

        l_error_message                := NULL ;  
        l_count                        := NULL ;
        lv_org_id                      := NULL ;
        lv_trx_type                    := NULL ;
        lv_term_name                   := NULL ;
        lv_orig_system_reference       := NULL ;
        lv_account_number              := NULL ;
        lv_party_orig_system_ref       := NULL ;
        lv_site_orig_system_reference  := NULL ;
        l_bill_to_cust_num             := NULL ;
        l_bill_to_cust_site_num        := NULL ;
 /*    -- Update STG table for CCSD by Sameer 
    UPDATE XX_RA_INT_LINES_ALL_STG
    SET    interface_line_attribute1 = LINE_TRX_FLEX_SEGMENT1_SEQ.nextval
          ,printing_option = 'N'
          ,interface_line_context = 'CCSD Conversion'
          ,memo_line_name = 'Conversion Memo Line' 
    WHERE rowid = line_rec.rowid     ;
    COMMIT;      */  
   --
   -- BU NAME Validation
       -- Business Unit
      IF line_rec.bu_name IS NULL
      THEN
          l_error_message :=  
                   l_error_message || 'Business Unit can not be NULL | ';
      ELSE
           SELECT NVL(count(*),0)
           INTO   l_count
           FROM   xx_bu_setup
           WHERE TRIM(bu_name)    = TRIM(line_rec.bu_name)
                  AND module_name = 'Receiving'
                  AND status      = 'A';
 --
           IF l_count = 0
           THEN
              l_error_message :=  
                   l_error_message || 'Invalid Business Unit Name | ';
           END IF;
      END IF;
 -- checck if Transaction Type is null
      IF line_rec.cust_trx_type_name IS NULL
      THEN
          l_error_message :=  
                   l_error_message || 'Transaction Type can not be NULL | ';
      ELSE
           SELECT NVL(count(*),0)
           INTO   l_count
           FROM   xx_cust_trx_types_master
           WHERE TRIM(name)    = TRIM(line_rec.cust_trx_type_name);
                --  AND status      = 'A';
 --
           IF l_count = 0
           THEN
              l_error_message :=  
                   l_error_message || 'Invalid Transaction Type | ';
           END IF;
      END IF;
 --     
 -- ORIG_SYSTEM_BILL_CUSTOMER_REF validation added by Sameer for CCSD Sprint2
 --
     IF line_rec.bill_customer_account_number IS NULL
     THEN
        BEGIN
           SELECT acct_ref
           INTO   l_bill_to_cust_num
           FROM   xx_ar_cust_site_setup
           WHERE  TRIM(ORIG_SYSTEM_BILL_TO_REF)  = TRIM(line_rec.ORIG_SYSTEM_BILL_CUSTOMER_REF);
        EXCEPTION
            WHEN OTHERS 
            THEN 
               l_bill_to_cust_num := NULL;
          END;
        
      -- check if bill to cust account is derived    
        IF l_bill_to_cust_num IS  NULL
        THEN 
          l_error_message :=  
                   l_error_message || 'Bill to Customer account number can not be NULL | ';
        ELSE 
          --sameer set Bill to cust number from Orig system reference
             UPDATE XX_RA_INT_LINES_ALL_STG
             SET    bill_customer_account_number = l_bill_to_cust_num ,
                   ORIG_SYSTEM_BILL_CUSTOMER_REF = NULL
             WHERE  --TRIM(TRX_NUMBER) = TRIM(line_rec.TRX_NUMBER);
                   rowid = line_rec.rowid;
             COMMIT;
        END IF;
      ELSE
         -- 
            SELECT NVL(count(*),0)
            INTO   l_count
            FROM   xx_ar_cust_site_setup
            WHERE TRIM(acct_ref)    = TRIM(line_rec.bill_customer_account_number);
 --
         IF l_count = 0
         THEN
            l_error_message :=  
                 l_error_message || 'Invalid Bill to Customer account number | ';
         END IF;
      END IF; 
      
 --     
 -- Bill-to Customer Site Number validation
 --
      IF line_rec.bill_customer_site_number IS NULL
      THEN
          BEGIN 
             SELECT site_ref
             INTO  l_bill_to_cust_site_num
             FROM  xx_ar_cust_site_setup
             WHERE  TRIM(ORIG_SYSTEM_BILL_TO_SITE_REF)    = TRIM(line_rec.ORIG_SYSTEM_BILL_ADDRESS_REF);
          EXCEPTION
            WHEN OTHERS 
            THEN 
               l_bill_to_cust_site_num := NULL;
          END;
      -- check if bill to cust site account is derived    
         IF l_bill_to_cust_site_num IS  NULL
         THEN 
            l_error_message :=  
                   l_error_message || 'Bill-to Customer Site Number can not be NULL | ';
         ELSE 
          --sameer set Bill to cust site number from Orig system reference
            UPDATE XX_RA_INT_LINES_ALL_STG
            SET    bill_customer_site_number = l_bill_to_cust_site_num ,
                   ORIG_SYSTEM_BILL_ADDRESS_REF = NULL           
            WHERE    rowid = line_rec.rowid;  --TRIM(TRX_NUMBER)  = TRIM(line_rec.TRX_NUMBER);
            COMMIT;
         END IF;
    --     
      ELSE
         SELECT NVL(count(*),0)
         INTO   l_count
         FROM   xx_ar_cust_site_setup
         WHERE TRIM(site_ref)    = TRIM(line_rec.bill_customer_site_number);
 --
         IF l_count = 0
         THEN
            l_error_message :=  
                 l_error_message || 'Invalid Bill-to Customer Site Number | ';
         END IF;
      END IF;   
 --     
 -- Bill-to Customer Site Number validation
 --    
      IF line_rec.term_name IS NULL
      THEN
          l_error_message :=  
                   l_error_message || 'Payment Terms can not be NULL | ';
      ELSE
         SELECT NVL(count(*),0)
         INTO   l_count
         FROM   xx_ar_terms_master
         WHERE TRIM(name)    = TRIM(line_rec.Term_Name);
 --
         IF l_count = 0
         THEN
         -- Check below validation for Init caps of Peyment terms by Sameer on 21-Apr-2020
           SELECT NVL(count(*),0)
           INTO   l_count_init
           FROM   xx_ar_terms_master
           WHERE  UPPER(TRIM(name))    = UPPER(TRIM(line_rec.Term_Name));
           IF l_count_init = 0
           THEN
               l_error_message :=  
                   l_error_message || 'Invalid Payment Terms | ';
           ELSE 
               update XX_RA_INT_LINES_ALL_STG
               SET    Term_Name =  initcap(TRIM(line_rec.Term_Name))
                WHERE  rowid = line_rec.rowid; --TRIM(TRX_NUMBER)  = TRIM(line_rec.TRX_NUMBER);
            COMMIT;
           END IF;        
         END IF;
      END IF;  

--                                                 IF line_rec.org_id IS NULL 
--                                                    THEN     
--                                                      
--                                                                   DBMS_OUTPUT.PUT_LINE ('inside line_rec.org_id IS NULL');
--                                                                     l_error_message := l_error_message ||  '||' ||   ' Organization ID  cannot be null ';
--                                                                        
--                                                    ELSE 
--                                                                   DBMS_OUTPUT.PUT_LINE ('inside ELSE OF line_rec.org_id IS NULL');
--
--                                                             BEGIN
--
--                                                                                  SELECT  organization_id  
--                                                                                  INTO  lv_org_id
--                                                                                  FROM XX_HR_ORGANIZATIONS_MASTER
--                                                                                  WHERE organization_id = line_rec.org_id ;
--
--                                                            EXCEPTION  WHEN NO_DATA_FOUND  
--                                                                         THEN     
--                                                                                                DBMS_OUTPUT.PUT_LINE ('inside NDF of XX_HR_ORGANIZATIONS_MASTER');
--                                                                                              l_error_message := l_error_message ||  '||' ||   'ORGANIZATION_ID '  || line_rec.org_id   ||   ' is not found in XX_HR_ORGANIZATIONS_MASTER ';
--
--                                                            END;
--                                                   END IF ;
--                                                   
--                                                 
--                                                  IF line_rec.ORIG_SYSTEM_BILL_CUSTOMER_REF IS NULL 
--                                                    THEN 
--                                                      
--                                                                   DBMS_OUTPUT.PUT_LINE ('inside line_rec.ORIG_SYSTEM_BILL_CUSTOMER_REF IS NULL ');
--                                                                     l_error_message := l_error_message ||  '||' ||   ' ORIG_SYSTEM_BILL_CUSTOMER_REF cannot be null ';
--                                                                        
--                                                    ELSE 
--                                                               BEGIN
--
--                                                                                      SELECT  name  
--                                                                                      INTO  lv_trx_type
--                                                                                      FROM XX_CUST_TRX_TYPES_MASTER
--                                                                                      WHERE name  = line_rec.CUST_TRX_TYPE_NAME ;
--
--                                                               EXCEPTION  WHEN NO_DATA_FOUND  
--                                                                             THEN     
--                                                                                                    DBMS_OUTPUT.PUT_LINE ('inside NDF of XX_CUST_TRX_TYPES_MASTER');
--                                                                                                  l_error_message := l_error_message ||  '||' ||   'Transaction Type '  || line_rec.CUST_TRX_TYPE_NAME   ||   ' is not found in XX_CUST_TRX_TYPES_MASTER ';
--
--                                                                END;
--                                                  END IF; 
--                                                    
----                                                    BEGIN
----                                                       DBMS_OUTPUT.PUT_LINE ('Before XX_AR_TERMAS_MASTER');
----
----                                                                          SELECT  name  
----                                                                          INTO  lv_term_name
----                                                                          FROM XX_AR_TERMAS_MASTER
----                                                                          WHERE name  = line_rec.TERM_NAME ;
----
----                                                     EXCEPTION  WHEN NO_DATA_FOUND  
----                                                                 THEN     
----                                                                                        DBMS_OUTPUT.PUT_LINE ('Inside NDF of XX_AR_TERMAS_MASTER');
----                                                                                      l_error_message := l_error_message ||  '||' ||   'Payment term '  || line_rec.TERM_NAME   ||   ' is not found in XX_AR_TERMAS_MASTER ';
----
----                                                    END;
--                                                    
--                                                    IF line_rec.ORIG_SYSTEM_BILL_CUSTOMER_REF IS NULL 
--                                                    THEN 
--                                                      
--                                                                   DBMS_OUTPUT.PUT_LINE ('line_rec.ORIG_SYSTEM_BILL_CUSTOMER_REF IS NULL');
--                                                                     l_error_message := l_error_message ||  '||' ||   ' ORIG_SYSTEM_BILL_CUSTOMER_REF cannot be null ';
--                                                                        
--                                                    ELSE 
--                                                             BEGIN
--
--                                                                                  SELECT  orig_system_reference, account_number 
--                                                                                  INTO  lv_orig_system_reference , lv_account_number 
--                                                                                  FROM XX_HZ_CUST_ACCOUNTS_MASTER
--                                                                                  WHERE orig_system_reference  = line_rec.ORIG_SYSTEM_BILL_CUSTOMER_REF;
--
--                                                             EXCEPTION  WHEN NO_DATA_FOUND  
--                                                                         THEN     
--                                                                                                DBMS_OUTPUT.PUT_LINE ('Inside NDF of XX_HZ_CUST_ACCOUNTS_MASTER');
--                                                                                              l_error_message := l_error_message ||  '||' ||   'ORIG_SYSTEM_BILL_CUSTOMER_REF '  ||  line_rec.ORIG_SYSTEM_BILL_CUSTOMER_REF   ||   ' is not found in XX_HZ_IMP_ACCOUNTS_T_STG ';
--
--                                                            END;
--                                                    END IF ;
--                                                    
--                                                    IF line_rec.ORIG_SYSTEM_BILL_ADDRESS_REF IS NOT  NULL 
--                                                    THEN 
--                                                      
--                                                             BEGIN
--
--                                                                                  SELECT  ORIG_SYSTEM_REFERENCE 
--                                                                                  INTO  lv_site_orig_system_reference 
--                                                                                  FROM XX_HZ_CUST_ACCT_SITES_MASTER
--                                                                                  WHERE ORIG_SYSTEM_REFERENCE   = line_rec.ORIG_SYSTEM_BILL_ADDRESS_REF;
--    
--
--                                                             EXCEPTION  WHEN NO_DATA_FOUND  
--                                                                         THEN     
--                                                                                                DBMS_OUTPUT.PUT_LINE ('Inside NDF of XX_HZ_CUST_ACCT_SITES_MASTER');
--                                                                                              l_error_message := l_error_message ||  '||' ||   'ORIG_SYSTEM_BILL_ADDRESS_REF '  ||  line_rec.ORIG_SYSTEM_BILL_ADDRESS_REF   ||   ' is not found in XX_HZ_IMP_ACCTSITES_T_STG ';
--
--                                                            END;
--                                                    END IF ;
--                                                 
--                                                                                               
        IF l_error_message IS NULL
        THEN 

            DBMS_OUTPUT.PUT_LINE ('NO ERRORS');
            DBMS_OUTPUT.PUT_LINE (line_rec.source_1); 
            UPDATE XX_RA_INT_LINES_ALL_STG 
            SET validation_flag = 'S'
             -- Update STG table for CCSD by Sameer Start 
               ,interface_line_attribute1 = LINE_TRX_FLEX_SEGMENT1_SEQ.nextval
           --    ,printing_option = 'N'
               ,interface_line_context = 'CCSD Conversion'
           --    ,memo_line_name = 'Conversion Memo Line'   
             -- Update STG table for CCSD by Sameer End
            WHERE rowid = line_rec.rowid;
            -- AND link_to_line_attribute2 = line_rec.link_to_line_attribute2
            -- AND  link_to_line_attribute3 = line_rec.link_to_line_attribute3 ;
            DBMS_OUTPUT.PUT_LINE ('COMMIED');
            COMMIT ;

        ELSE 
            DBMS_OUTPUT.PUT_LINE ('ERRORSSS');
            UPDATE XX_RA_INT_LINES_ALL_STG 
            SET validation_flag = 'E',
            PROCESS_FLAG = 'F' ,
            error_message = l_error_message
            WHERE rowid = line_rec.rowid;
            -- AND link_to_line_attribute2 = line_rec.link_to_line_attribute2
            --AND  link_to_line_attribute3 = line_rec.link_to_line_attribute3 ; 

            COMMIT ;

        END IF;

    END LOOP;

    COMMIT;


         INSERT INTO XX_RA_INT_LINES_ALL_VO
         SELECT *
            FROM XX_RA_INT_LINES_ALL_STG
         WHERE validation_flag = 'S'; 
         COMMIT;		


             END invoice_line_validate;

         /*    PROCEDURE   invoice_distributions_validate ( source_1 IN VARCHAR2)   AS 

             CURSOR dist_cur IS
             SELECT ROWID,stg1.* 
             FROM XX_RA_INT_DIST_ALL_STG stg1
             WHERE source_1 = source_1
             AND nvl(validation_flag, 'N') != 'S'
             and exists (select 'X' FROM XX_RA_INT_LINES_ALL_STG xrla
             WHERE xrla.validation_flag = 'S'
             and stg1.INTERFACE_LINE_CONTEXT     = xrla.INTERFACE_LINE_CONTEXT 
             and  stg1.INTERFACE_LINE_ATTRIBUTE1  = xrla.INTERFACE_LINE_ATTRIBUTE1  
             and stg1.INTERFACE_LINE_ATTRIBUTE2  = xrla.INTERFACE_LINE_ATTRIBUTE2); 
            -- and stg1.INTERFACE_LINE_ATTRIBUTE3  = xrla.INTERFACE_LINE_ATTRIBUTE3  
            -- and stg1.INTERFACE_LINE_ATTRIBUTE4 = xrla.INTERFACE_LINE_ATTRIBUTE4);


             CURSOR dist_err IS
             SELECT *  from XX_RA_INT_LINES_ALL_STG
             where validation_flag='E';

            -- order by interface_line_attribute2 , interface_line_attribute3; 

              l_error_message VARCHAR2(5000) ;


             BEGIN

                           FOR dist_rec IN dist_cur
                                   LOOP 

                                                 l_error_message := NULL;


                                              IF l_error_message IS NULL
                                              THEN 

                                                       DBMS_OUTPUT.PUT_LINE ('Output6');

                                                       UPDATE XX_RA_INT_DIST_ALL_STG 
                                                      SET validation_flag = 'S'
                                                      WHERE rowid = dist_rec.rowid;
                                                     -- AND interface_line_attribute2 = dist_rec.interface_line_attribute2
                                                      --AND  interface_line_attribute3 = dist_rec.interface_line_attribute3 ;

   INSERT INTO XX_RA_INT_DIST_ALL_VO 
   VALUES
  (dist_rec.SOURCE_1                    
  ,dist_rec.STAGING_RUN_ID               
  ,dist_rec.ORG_ID                      
  ,dist_rec.ACCOUNT_CLASS               
  ,dist_rec.AMOUNT                      
  ,dist_rec.PERCENT                    
  ,dist_rec.ACCTD_AMOUNT                
  ,dist_rec.INTERFACE_LINE_CONTEXT     
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE1  
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE2  
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE3  
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE4  
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE5  
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE6  
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE7  
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE8  
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE9  
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE10 
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE11 
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE12 
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE13 
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE14 
  ,dist_rec.INTERFACE_LINE_ATTRIBUTE15 
--  ,dist_rec.segment2
--  ,dist_rec.segment1
--  ,dist_rec.segment4
--  ,dist_rec.segment5
--  ,dist_rec.segment6
--  ,dist_rec.segment7
--  ,dist_rec.segment8
--  ,dist_rec.segment3
--  ,dist_rec.segment9           
  ,dist_rec.segment1
  ,dist_rec.segment2
  ,dist_rec.segment3
  ,dist_rec.segment4
  ,dist_rec.segment5
  ,dist_rec.segment6
  ,dist_rec.segment7
  ,dist_rec.segment8
  ,dist_rec.segment9           
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL
  ,NULL     
  ,NULL
  ,NULL           
  ,dist_rec.COMMENTS                    
  ,dist_rec.INTERIM_TAX_SEGMENT1       
  ,dist_rec.INTERIM_TAX_SEGMENT2       
  ,dist_rec.INTERIM_TAX_SEGMENT3       
  ,dist_rec.INTERIM_TAX_SEGMENT4       
  ,dist_rec.INTERIM_TAX_SEGMENT5       
  ,dist_rec.INTERIM_TAX_SEGMENT6       
  ,dist_rec.INTERIM_TAX_SEGMENT7       
  ,dist_rec.INTERIM_TAX_SEGMENT8       
  ,dist_rec.INTERIM_TAX_SEGMENT9       
  ,dist_rec.INTERIM_TAX_SEGMENT10      
  ,dist_rec.INTERIM_TAX_SEGMENT11      
  ,dist_rec.INTERIM_TAX_SEGMENT12      
  ,dist_rec.INTERIM_TAX_SEGMENT13      
  ,dist_rec.INTERIM_TAX_SEGMENT14      
  ,dist_rec.INTERIM_TAX_SEGMENT15      
  ,dist_rec.INTERIM_TAX_SEGMENT16      
  ,dist_rec.INTERIM_TAX_SEGMENT17      
  ,dist_rec.INTERIM_TAX_SEGMENT18      
  ,dist_rec.INTERIM_TAX_SEGMENT19      
  ,dist_rec.INTERIM_TAX_SEGMENT20      
  ,dist_rec.INTERIM_TAX_SEGMENT21      
  ,dist_rec.INTERIM_TAX_SEGMENT22      
  ,dist_rec.INTERIM_TAX_SEGMENT23      
  ,dist_rec.INTERIM_TAX_SEGMENT24      
  ,dist_rec.INTERIM_TAX_SEGMENT25      
  ,dist_rec.INTERIM_TAX_SEGMENT26      
  ,dist_rec.INTERIM_TAX_SEGMENT27      
  ,dist_rec.INTERIM_TAX_SEGMENT28      
  ,dist_rec.INTERIM_TAX_SEGMENT29      
  ,dist_rec.INTERIM_TAX_SEGMENT30      
  ,dist_rec.ATTRIBUTE_CATEGORY         
  ,dist_rec.ATTRIBUTE1                  
  ,dist_rec.ATTRIBUTE2                  
  ,dist_rec.ATTRIBUTE3                  
  ,dist_rec.ATTRIBUTE4                  
  ,dist_rec.ATTRIBUTE5                  
  ,dist_rec.ATTRIBUTE6                  
  ,dist_rec.ATTRIBUTE7                  
  ,dist_rec.ATTRIBUTE8                  
  ,dist_rec.ATTRIBUTE9                  
  ,dist_rec.ATTRIBUTE10                 
  ,dist_rec.ATTRIBUTE11                 
  ,dist_rec.ATTRIBUTE12                 
  ,dist_rec.ATTRIBUTE13                 
  ,dist_rec.ATTRIBUTE14                 
  ,dist_rec.ATTRIBUTE15                 
  ,dist_rec.BU_NAME                 
  ,dist_rec.CREATED_BY                 
  ,SYSDATE
  ,dist_rec.LAST_UPDATED_BY
  ,SYSDATE
  ,'N'
  ,'S'
  ,'N'
  );     

                                                      COMMIT ;

                                             ELSE 
                                                            DBMS_OUTPUT.PUT_LINE ('Output7');
                                                     UPDATE XX_RA_INT_DIST_ALL_STG 
                                                      SET validation_flag = 'E',
                                                      PROCESS_FLAG = 'F' ,
                                                      error_message = l_error_message
                                                      WHERE  rowid = dist_rec.rowid;
                                                   --   AND interface_line_attribute2 = dist_rec.interface_line_attribute2
                                                    --  AND  interface_line_attribute3 = dist_rec.interface_line_attribute3 ; 




                                                      COMMIT ;

                                             END IF;


                                   END LOOP;

                                 COMMIT; 



             END invoice_distributions_validate;

             PROCEDURE  salescredits_validate ( source_1 IN VARCHAR2)     AS 

            CURSOR salescredit_cur IS
             SELECT * 
             FROM XX_RA_INT_SALESCREDITS_STG
             WHERE  source_1 = source_1
             AND nvl(validation_flag, 'N') != 'S'
             order by interface_line_attribute2 , interface_line_attribute3; 

              l_error_message VARCHAR2(5000) ;


             BEGIN

                           FOR salescredit_rec IN salescredit_cur
                                   LOOP 

                                                 l_error_message := NULL;


                                              IF l_error_message IS NULL
                                              THEN 

                                                       DBMS_OUTPUT.PUT_LINE ('Output6');

                                                       UPDATE XX_RA_INT_SALESCREDITS_STG 
                                                      SET validation_flag = 'S'
                                                      WHERE  source_1 = salescredit_rec.source_1
                                                      AND interface_line_attribute2 = salescredit_rec.interface_line_attribute2
                                                      AND  interface_line_attribute3 = salescredit_rec.interface_line_attribute3 ;

                                                      COMMIT ;

                                             ELSE 
                                                            DBMS_OUTPUT.PUT_LINE ('Output7');
                                                     UPDATE XX_RA_INT_SALESCREDITS_STG 
                                                      SET validation_flag = 'E',
                                                      PROCESS_FLAG = 'F' ,
                                                      error_message = l_error_message
                                                      WHERE source_1 = salescredit_rec.source_1
                                                      AND interface_line_attribute2 = salescredit_rec.interface_line_attribute2
                                                      AND  interface_line_attribute3 = salescredit_rec.interface_line_attribute3 ; 

                                                       COMMIT ;

                                             END IF;

                                   END LOOP;

                                 COMMIT;

			     INSERT INTO XX_RA_INT_SALESCREDITS_STG
         SELECT *
            FROM XX_RA_INT_SALESCREDITS_STG
         WHERE validation_flag = 'S';       					 
             	COMMIT;				 

             END  salescredits_validate ;

             PROCEDURE  line_contingencies_validate ( source_1 IN VARCHAR2)     AS 

             CURSOR contingencies_cur IS
             SELECT * 
             FROM XX_AR_INT_CONTS_ALL_STG
             WHERE source_1 = source_1
             AND nvl(validation_flag, 'N') != 'S'
             order by interface_line_attribute2 , interface_line_attribute3; 

              l_error_message VARCHAR2(5000) ;


             BEGIN

                           FOR contingencies_rec IN contingencies_cur
                                   LOOP 

                                                 l_error_message := NULL;


                                              IF l_error_message IS NULL
                                              THEN 

                                                       DBMS_OUTPUT.PUT_LINE ('Output6');

                                                       UPDATE XX_AR_INT_CONTS_ALL_STG 
                                                      SET validation_flag = 'S'
                                                      WHERE source_1 =  contingencies_rec.source_1
                                                      AND interface_line_attribute2 = contingencies_rec.interface_line_attribute2
                                                      AND  interface_line_attribute3 = contingencies_rec.interface_line_attribute3 ;

                                                      COMMIT ;

                                             ELSE 
                                                            DBMS_OUTPUT.PUT_LINE ('Output7');
                                                     UPDATE XX_AR_INT_CONTS_ALL_STG 
                                                      SET validation_flag = 'E',
                                                      PROCESS_FLAG = 'F' ,
                                                      error_message = l_error_message
                                                      WHERE source_1 =  contingencies_rec.source_1
                                                      AND interface_line_attribute2 = contingencies_rec.interface_line_attribute2
                                                      AND  interface_line_attribute3 = contingencies_rec.interface_line_attribute3 ; 

                                                       COMMIT ;



                                             END IF;

                                   END LOOP;

                                 COMMIT;


                 INSERT INTO XX_AR_INT_CONTS_ALL_VO
         SELECT *
            FROM XX_AR_INT_CONTS_ALL_STG
         WHERE validation_flag = 'S'; 
         COMMIT;		 
             END line_contingencies_validate; 
*/
 END XX_AR_INVOICE_VALIDATE;