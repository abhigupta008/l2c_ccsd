--------------------------------------------------------
--  DDL for Table XX_EGP_ITEM_TRANS_TYPE_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_EGP_ITEM_TRANS_TYPE_SETUP" 
   (	"TRANSACTION_TYPE_ID" NUMBER, 
	"TRANSACTION_TYPE_NAME" VARCHAR2(20), 
	"DESCRIPTION" VARCHAR2(20), 
	"CREATED_BY" VARCHAR2(20), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(20), 
	"LAST_UPDATE_DATE" DATE
   ) ;
