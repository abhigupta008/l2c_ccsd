create or replace PACKAGE XX_AR_INVOICE_VALIDATE AS
     
            PROCEDURE  invoice_validate ( source_1 			IN 		 VARCHAR2,
						x_errbuf            OUT      VARCHAR2,
						x_retcode           OUT      NUMBER
						);

             PROCEDURE  invoice_line_validate ( source_1 IN VARCHAR2);
             
         /*     PROCEDURE   invoice_distributions_validate ( source_1 IN VARCHAR2);
             
             PROCEDURE  SALESCREDITS_validate ( source_1 IN VARCHAR2) ;
             
             PROCEDURE  line_contingencies_validate ( source_1 IN VARCHAR2) ; */
             
 END XX_AR_INVOICE_VALIDATE;