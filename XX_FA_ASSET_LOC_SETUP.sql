--------------------------------------------------------
--  DDL for Table XX_FA_ASSET_LOC_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_FA_ASSET_LOC_SETUP" 
   (	"LOCATION_CODE" VARCHAR2(100), 
	"SEGMENT1" VARCHAR2(30), 
	"DESCRIPTION" VARCHAR2(100), 
	"ENABLED_FLAG" VARCHAR2(1), 
	"SUMMARY_FLAG" VARCHAR2(1), 
	"POSTING_ALLOWED" VARCHAR2(1), 
	"BUDGETING_ALLOWED" VARCHAR2(1), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
