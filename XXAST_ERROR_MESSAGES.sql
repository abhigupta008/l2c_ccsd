--------------------------------------------------------
--  DDL for Table XXAST_ERROR_MESSAGES
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XXAST_ERROR_MESSAGES" 
   (	"ID" NUMBER, 
	"ERROR_KEY" VARCHAR2(360), 
	"ERROR_MESSAGE" VARCHAR2(360), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
