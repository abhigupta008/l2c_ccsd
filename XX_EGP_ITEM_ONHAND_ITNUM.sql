--------------------------------------------------------
--  DDL for Table XX_EGP_ITEM_ONHAND_ITNUM
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_EGP_ITEM_ONHAND_ITNUM" 
   (	"INV_ORG_ID" NUMBER, 
	"ITEM_NUMBER" VARCHAR2(250), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
