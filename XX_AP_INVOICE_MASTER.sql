--------------------------------------------------------
--  DDL for Table XX_AP_INVOICE_MASTER
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_AP_INVOICE_MASTER" 
   (	"INVOICE_ID" NUMBER, 
	"INVOICE_NUM" VARCHAR2(80), 
	"INVOICE_DATE" DATE, 
	"SOURCE" VARCHAR2(80), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
