--------------------------------------------------------
--  DDL for Table HZ_IMP_PARTYSITES_T_STG
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."HZ_IMP_PARTYSITES_T_STG" 
   (	"SOURCE_1" VARCHAR2(80), 
	"STAGING_RUN_ID" NUMBER, 
	"BATCH_ID" NUMBER(18,0), 
	"PARTY_ORIG_SYSTEM" VARCHAR2(30), 
	"PARTY_ORIG_SYSTEM_REFERENCE" VARCHAR2(240), 
	"SITE_ORIG_SYSTEM" VARCHAR2(30), 
	"SITE_ORIG_SYSTEM_REFERENCE" VARCHAR2(240), 
	"LOCATION_ORIG_SYSTEM" VARCHAR2(30), 
	"LOCATION_ORIG_SYSTEM_REFERENCE" VARCHAR2(240), 
	"INSERT_UPDATE_FLAG" VARCHAR2(1), 
	"PARTY_SITE_NAME" VARCHAR2(240), 
	"PARTY_SITE_NUMBER" VARCHAR2(30), 
	"START_DATE_ACTIVE" DATE, 
	"END_DATE_ACTIVE" DATE, 
	"MAILSTOP" VARCHAR2(60), 
	"IDENTIFYING_ADDRESS_FLAG" VARCHAR2(1), 
	"PARTY_SITE_LANGUAGE" VARCHAR2(4), 
	"ATTRIBUTE_CATEGORY" VARCHAR2(30), 
	"ATTRIBUTE1" VARCHAR2(150), 
	"ATTRIBUTE2" VARCHAR2(150), 
	"ATTRIBUTE3" VARCHAR2(150), 
	"ATTRIBUTE4" VARCHAR2(150), 
	"ATTRIBUTE5" VARCHAR2(150), 
	"ATTRIBUTE6" VARCHAR2(150), 
	"ATTRIBUTE7" VARCHAR2(150), 
	"ATTRIBUTE8" VARCHAR2(150), 
	"ATTRIBUTE9" VARCHAR2(150), 
	"ATTRIBUTE10" VARCHAR2(150), 
	"ATTRIBUTE11" VARCHAR2(150), 
	"ATTRIBUTE12" VARCHAR2(150), 
	"ATTRIBUTE13" VARCHAR2(150), 
	"ATTRIBUTE14" VARCHAR2(150), 
	"ATTRIBUTE15" VARCHAR2(150), 
	"ATTRIBUTE16" VARCHAR2(150), 
	"ATTRIBUTE17" VARCHAR2(150), 
	"ATTRIBUTE18" VARCHAR2(150), 
	"ATTRIBUTE19" VARCHAR2(150), 
	"ATTRIBUTE20" VARCHAR2(150), 
	"ATTRIBUTE21" VARCHAR2(150), 
	"ATTRIBUTE22" VARCHAR2(150), 
	"ATTRIBUTE23" VARCHAR2(150), 
	"ATTRIBUTE24" VARCHAR2(150), 
	"ATTRIBUTE25" VARCHAR2(150), 
	"ATTRIBUTE26" VARCHAR2(150), 
	"ATTRIBUTE27" VARCHAR2(150), 
	"ATTRIBUTE28" VARCHAR2(150), 
	"ATTRIBUTE29" VARCHAR2(150), 
	"ATTRIBUTE30" VARCHAR2(150), 
	"REL_ORIG_SYSTEM" VARCHAR2(30), 
	"REL_ORIG_SYSTEM_REFERENCE" VARCHAR2(240), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE, 
	"VALIDATION_FLAG" VARCHAR2(1), 
	"PROCESS_FLAG" VARCHAR2(1), 
	"ERROR_MESSAGE" VARCHAR2(2000)
   ) ;
