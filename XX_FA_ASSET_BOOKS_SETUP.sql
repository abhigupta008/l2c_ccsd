--------------------------------------------------------
--  DDL for Table XX_FA_ASSET_BOOKS_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_FA_ASSET_BOOKS_SETUP" 
   (	"BOOK_TYPE_CODE" VARCHAR2(80), 
	"BOOK_NAME" VARCHAR2(80), 
	"BOOK_CLASS" VARCHAR2(80), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
