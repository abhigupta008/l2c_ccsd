--------------------------------------------------------
--  DDL for Table XX_AP_SUPPLIER_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_AP_SUPPLIER_SETUP" 
   (	"VENDOR_NAME" VARCHAR2(360), 
	"SEGMENT1" VARCHAR2(30), 
	"VENDOR_TYPE_LOOKUP_CODE" VARCHAR2(30), 
	"ONE_TIME_SUPPLIER_FLAG" VARCHAR2(1), 
	"SUPPLIER_START_DATE" DATE, 
	"SUPPLIER_END_DATE" DATE, 
	"INCOME_TAX_ID" VARCHAR2(100), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
