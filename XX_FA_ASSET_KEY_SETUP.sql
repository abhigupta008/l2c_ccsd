--------------------------------------------------------
--  DDL for Table XX_FA_ASSET_KEY_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_FA_ASSET_KEY_SETUP" 
   (	"CODE_COMBINATION_ID" NUMBER, 
	"SEGMENT1" VARCHAR2(30), 
	"SUMMARY_FLAG" VARCHAR2(1), 
	"ENABLED_FLAG" VARCHAR2(1), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
