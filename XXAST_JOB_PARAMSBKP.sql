--------------------------------------------------------
--  DDL for Table XXAST_JOB_PARAMSBKP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XXAST_JOB_PARAMSBKP" 
   (	"ID" NUMBER, 
	"INTERFACE_KEY" VARCHAR2(360), 
	"JOB_TYPE" VARCHAR2(360), 
	"PARAM_NO" VARCHAR2(360), 
	"PRARAM_VALUE" VARCHAR2(360), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE, 
	"DESCRIPTION" VARCHAR2(255 CHAR)
   ) ;
