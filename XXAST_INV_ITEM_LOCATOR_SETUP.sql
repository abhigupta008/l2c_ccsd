--------------------------------------------------------
--  DDL for Table XXAST_INV_ITEM_LOCATOR_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XXAST_INV_ITEM_LOCATOR_SETUP" 
   (	"SUBINVENTORY_CODE" VARCHAR2(100), 
	"SEGMENT1" VARCHAR2(240), 
	"SEGMENT2" VARCHAR2(240), 
	"SEGMENT3" VARCHAR2(240), 
	"CREATED_BY" VARCHAR2(240), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(240), 
	"LAST_UPDATE_DATE" DATE
   ) ;
