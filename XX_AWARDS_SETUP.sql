--------------------------------------------------------
--  DDL for Table XX_AWARDS_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_AWARDS_SETUP" 
   (	"AWARD_NAME" VARCHAR2(300), 
	"AWARD_NUMBER" VARCHAR2(120), 
	"CREATED_BY" VARCHAR2(150), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(150), 
	"LAST_UPDATED_DATE" DATE
   ) ;
