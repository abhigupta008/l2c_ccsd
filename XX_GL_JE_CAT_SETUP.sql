--------------------------------------------------------
--  DDL for Table XX_GL_JE_CAT_SETUP
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_GL_JE_CAT_SETUP" 
   (	"JE_CATEGORY_NAME" VARCHAR2(25), 
	"JE_CATEGORY_KEY" VARCHAR2(25), 
	"DESCRIPTION" VARCHAR2(240), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
