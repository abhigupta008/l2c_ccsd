--------------------------------------------------------
--  DDL for Table XXAST_BI_CONFIG
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XXAST_BI_CONFIG" 
   (	"ID" NUMBER, 
	"INTERFACE_KEY" VARCHAR2(360), 
	"BI_PATH" VARCHAR2(360), 
	"BI_TABLE" VARCHAR2(360), 
	"STATUS" VARCHAR2(360), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
