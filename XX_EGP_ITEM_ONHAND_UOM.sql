--------------------------------------------------------
--  DDL for Table XX_EGP_ITEM_ONHAND_UOM
--------------------------------------------------------

  CREATE TABLE "SYSTEM"."XX_EGP_ITEM_ONHAND_UOM" 
   (	"UOM_CODE" VARCHAR2(250), 
	"UOM_DESCRIPTION" VARCHAR2(250), 
	"UOM" VARCHAR2(250), 
	"CREATED_BY" VARCHAR2(30), 
	"CREATION_DATE" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(30), 
	"LAST_UPDATE_DATE" DATE
   ) ;
